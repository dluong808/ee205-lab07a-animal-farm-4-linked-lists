///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file list.hpp
/// @version 1.0
///
/// Singly linked list implementation
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @March 25 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

namespace animalfarm {

class SingleLinkedList {
   protected:
      Node* head = nullptr;
      Node* tempNode = nullptr;
   public: 
      const bool empty() const;
      void push_front( Node* newNode );
      Node* pop_front();
      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
      unsigned int size() const;
};

}//namespace animalfarm
