///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file node.hpp
/// @version 1.0
///
/// Node header file
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @March 25  2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

namespace animalfarm{

class Node {

   friend class SingleLinkedList;
   protected:
      Node* next = nullptr;
};

}
