///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file list.cpp
/// @version 1.0
///
/// Singly linked list
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @March 25 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "list.hpp"

namespace animalfarm {

// Checks if list is empty
const bool SingleLinkedList::empty() const{
   if (head == nullptr)
      return true;
   else return false;
};

// Newnode becomes the head and the old head is the next pointer
void SingleLinkedList::push_front (Node* newNode){

   newNode->next=head;
   head=newNode;
   
};

//Saves old head pointer to a temp to return to main and iterates head to
//be the next pointer
Node* SingleLinkedList::pop_front(){
   if(head != nullptr){
      tempNode =head;
      head=head->next;
   }
   return tempNode;
};

//Returns the head pointer the first one
Node* SingleLinkedList::get_first() const {
   return head;
};

// Returns the next node
Node* SingleLinkedList::get_next( const Node* currentNode ) const
{
   return currentNode->next;
};

//Counts how many times it goes from node to node
unsigned int SingleLinkedList::size() const{
   int i=0;
    Node* temp = head;
   while(temp != nullptr){
      temp=temp->next;
      i++;
   }
   return i;
};

} //namespace animalfarm
