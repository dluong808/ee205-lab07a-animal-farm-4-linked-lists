///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @February 13 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku(  float newWeight , enum Color newColor, enum Gender newGender ){
   gender = newGender;

   species = "Aku";

   scaleColor = newColor;

   favoriteTemp = 99.99;

   weight = newWeight; 

}

const string Aku::speak() {
   return Fish::speak();
}

   void Aku::printInfo(){
      cout << "Weight = [" << weight << "]" << endl;
      Fish::printInfo();
   }

} //namespace animalfarm
